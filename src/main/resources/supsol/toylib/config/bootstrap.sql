--This SQL script is run every time the application starts. This means that data is not retained between
--restarts of tomcat, but this is intended until after user acceptance testing. An in-memory database allows for
--consistent functional testing and independence from database setup which can be done in parallel. An external database
--can easily be used when going into final pre-production testing by changing the data source in AppConfig.java.

--create our tables
CREATE TABLE category (
    category_id INT AUTO_INCREMENT PRIMARY KEY,
    name varchar(255));

CREATE TABLE library (
    library_id INT AUTO_INCREMENT PRIMARY KEY,
    name varchar(255) NOT NULL,
    address varchar(255) NOT NULL,
    phone_num VARCHAR(255) NOT NULL);

CREATE TABLE toy (
    toy_id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    category_id INT REFERENCES category(category_id),
    last_library_id INT REFERENCES library(library_id),
    market_value FLOAT NOT NULL);

CREATE TABLE lib_user (
    user_id INT AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(255) UNIQUE NOT NULL,
    password_hash VARCHAR(255) NOT NULL,
    full_name VARCHAR(255) NOT NULL,
    address VARCHAR(255) NOT NULL,
    phone_num VARCHAR(255) NOT NULL,
    staff BOOLEAN NOT NULL,
    fees FLOAT NOT NULL);

CREATE TABLE booking (
    booking_id INT AUTO_INCREMENT PRIMARY KEY,
    toy_id INT REFERENCES toy(toy_id) ON DELETE CASCADE,
    pickup_library_id INT REFERENCES library(library_id) ON DELETE CASCADE,
    pickup_date DATE NOT NULL,
    return_date DATE NOT NULL,
    user_id INT REFERENCES lib_user(user_id) ON DELETE CASCADE,
    status VARCHAR(15) NOT NULL);

--example categories
INSERT INTO category (category_id, name) VALUES (1, 'Babies (1-2 years)');
INSERT INTO category (category_id, name) VALUES (2, 'Girls (3-6 years)');
INSERT INTO category (category_id, name) VALUES (3, 'Boys (3-6 years)');

--example libraries
INSERT INTO library (library_id, name, address, phone_num)
VALUES (1, 'Gordon Head Branch', '123 Gordon Head Rd.', '(250) 555 1234');

INSERT INTO library (library_id, name, address, phone_num)
VALUES (2, 'Royal Oak Branch', '456 Wilkinson Rd.', '(250) 555 5432');

--example toys
INSERT INTO toy (toy_id, name, category_id, last_library_id, market_value)
VALUES (1, 'Large building blocks', 1, 1, 19.95);

INSERT INTO toy (toy_id, name, category_id, last_library_id, market_value)
VALUES (2, 'Shape sorter', 1, 2, 25.99);

INSERT INTO toy (toy_id, name, category_id, last_library_id, market_value)
VALUES (3, 'Dora the Explorer doll', 2, 1, 3.25);

INSERT INTO toy (toy_id, name, category_id, last_library_id, market_value)
VALUES (4, 'Ballerina figurine', 2, 2, 10.12);

--example users
--the password_hash is the hash of "password"
INSERT INTO lib_user (user_id, email, password_hash, full_name, address, phone_num, staff, fees)
VALUES (1, 'staff@toylib.com', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 'John Doe', '123 Fake St.', '(250) 555 1234', TRUE, 0.0);

