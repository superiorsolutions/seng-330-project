package supsol.toylib.view;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import supsol.toylib.entity.Booking;
import supsol.toylib.entity.Library;
import supsol.toylib.entity.Toy;
import supsol.toylib.service.LibraryService;
import supsol.toylib.service.ToyService;
import supsol.toylib.service.booking.*;
import supsol.toylib.service.user.UserSession;
import supsol.toylib.view.formbacker.CreateBookingBacker;
import supsol.toylib.view.formbacker.EditBookingBacker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/* As a @Controller, this class is responsible for handling requests forwarded in by the servlet container (tomcat)
 * The BoookingView handles requests on the /booking url and is mainly responsible for displaying pages about bookings.
 * It is also responsible for handling POST requests as a result of form submissions.
 */
@Controller
@RequestMapping("/booking")
public class BookingView {

    @Autowired
    private BookingService bookingService;
    @Autowired
    private ToyService toyService;
    @Autowired
    private UserSession userSession;
    @Autowired
    private LibraryService libraryService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ModelAndView doCreateBooking(@ModelAttribute("backer") CreateBookingBacker backer) {
        if (!userSession.isLoggedIn()) {
            return new ModelAndView("redirect:/login");
        }

        Date pickupDate;
        Date returnDate;

        //simple validating
        try {
            pickupDate = DateUtils.parseDate(backer.getPickupDate(), new String[] {"dd/MM/yy"});
            returnDate = DateUtils.parseDate(backer.getReturnDate(), new String[] {"dd/MM/yy"});
        } catch (ParseException e) {
            return new ModelAndView("main/error", "errormsg", "The given date(s) are not the correct format.");
        }
        Date today = DateUtils.truncate(new Date(), Calendar.DATE);
        if (!returnDate.after(pickupDate)) {
            return new ModelAndView("main/error", "errormsg", "The dropoff date must be after the pickup date.");
        }
        if (pickupDate.before(today) || returnDate.before(today)) {
            return new ModelAndView("main/error", "errormsg", "Bookings cannot be created in the past.");
        }

        final Booking newBooking = new Booking(
                backer.getToyId(),
                backer.getPickupLibraryId(),
                pickupDate,
                returnDate,
                backer.getUserId(),
                Booking.BookingStatus.TENTATIVE);

        //booking creation and catching business logic validation failures
        try {
            if (backer.isMaintenance()) {
                bookingService.createMaintenanceBooking(newBooking);
            } else {
                bookingService.createBooking(newBooking);
            }
        } catch (AccountSuspendedException e) {
            return new ModelAndView("main/error", "errormsg", "You cannot create bookings because you have returned " +
                    e.getLateReturns().size() + " bookings late within the last " + e.getSuspensionPeriod() + " days.");
        } catch (OverdueBookingsException e) {
            return new ModelAndView("main/error", "errormsg", "You cannot create bookings because you have " +
                    e.getOverdueBookings().size() + " overdue booking(s).");
        } catch (BookingLimitExceededException e) {
            return new ModelAndView("main/error", "errormsg", "You cannot book this toy for more than " + e.getLimit() +
                    " days per year");
        } catch (CannotMoveToyException e) {
            return new ModelAndView("main/error", "errormsg", "If the pickup library is not the same as the toy's current library, you must allow " +
                    e.getRequiredDays() + " day(s) for staff to move the toy.");
        }

        //todo: figure out how to get these model attributes forwarded through the viewtoy booking form instead of requerying them
        final Toy toy = toyService.getToy(newBooking.getToyId());
        final Library library = libraryService.getLibrary(newBooking.getPickupLibraryId());

        return new ModelAndView("booking/bookingconfirm", new HashMap<String, Object>() {{
            put("booking", newBooking);
            put("toy", toy);
            put("library", library);
        }});
    }

    @RequestMapping("/edit")
    public ModelAndView showEditBooking(@RequestParam("id") int bookingId) {
        if (!userSession.isStaff()) {
            return new ModelAndView("redirect:/login");
        }

        Booking booking = bookingService.getBooking(bookingId);
        if (booking == null) {
            return new ModelAndView("main/error", "errormsg", "That booking does not exist.");
        }

        //set up the form backer
        final EditBookingBacker backer = new EditBookingBacker();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
        backer.setBookingId(booking.getId());
        backer.setPickupDate(sdf.format(booking.getPickupDate()));
        backer.setReturnDate(sdf.format(booking.getReturnDate()));
        backer.setPickupLibraryId(booking.getPickupLibraryId());
        backer.setStatus(booking.getStatus().name());

        return new ModelAndView("booking/editbooking", new HashMap<String, Object>() {{
            put("libraries", libraryService.getAllLibraries());
            put("statuses", Booking.BookingStatus.values());
            put("backer", backer);
        }});
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ModelAndView doEditBooking(@ModelAttribute("backer") EditBookingBacker backer) {
        if (!userSession.isStaff()) {
            return new ModelAndView("redirect:/login");
        }

        Date pickupDate;
        Date returnDate;

        //simple validating
        try {
            pickupDate = DateUtils.parseDate(backer.getPickupDate(), new String[] {"dd/MM/yy"});
            returnDate = DateUtils.parseDate(backer.getReturnDate(), new String[] {"dd/MM/yy"});
        } catch (ParseException e) {
            return new ModelAndView("main/error", "errormsg", "The given date(s) are not the correct format.");
        }
        Date today = DateUtils.truncate(new Date(), Calendar.DATE);
        if (!returnDate.after(pickupDate)) {
            return new ModelAndView("main/error", "errormsg", "The dropoff date must be after the pickup date.");
        }
        if (pickupDate.before(today) || returnDate.before(today)) {
            return new ModelAndView("main/error", "errormsg", "Bookings cannot be created in the past.");
        }

        Booking booking = bookingService.getBooking(backer.getBookingId());
        booking.setPickupDate(pickupDate);
        booking.setReturnDate(returnDate);
        booking.setPickupLibraryId(backer.getPickupLibraryId());
        booking.setStatus(Booking.BookingStatus.valueOf(backer.getStatus()));
        bookingService.updateBooking(booking);

        return new ModelAndView("redirect:/booking/tentative");
    }

    @RequestMapping(value = "/cancel", method = RequestMethod.POST)
    public String doCancelBooking(@RequestParam("id") int bookingId) {
        Booking booking = bookingService.getBooking(bookingId);
        booking.setStatus(Booking.BookingStatus.CANCELLED);
        bookingService.updateBooking(booking);

        return "redirect:/";
    }

    @RequestMapping("/tentative")
    public ModelAndView showTentativeBookingsList() {
        if (!userSession.isStaff()) {
            return new ModelAndView("redirect:/login");
        }
        List<Booking> tentativeBookings = bookingService.getTentativeBookings();
        return new ModelAndView("booking/tentativelist", "bookings", tentativeBookings);
    }

    @ExceptionHandler
    public ModelAndView showError(Exception e) {
        //if we know the user is a staff member, we can show them a more detailed error
        return new ModelAndView("main/error", "errormsg",
                userSession.isStaff() ? e.getMessage() : "Oops! There was an internal error. Please try again.");
    }
}
