package supsol.toylib.view.formbacker;

//pojo used to back values in an html form
public class EditBookingBacker {
    private int bookingId;
    private int pickupLibraryId;
    private String pickupDate;
    private String returnDate;
    private String status;

    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    public int getPickupLibraryId() {
        return pickupLibraryId;
    }

    public void setPickupLibraryId(int pickupLibraryId) {
        this.pickupLibraryId = pickupLibraryId;
    }

    public String getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
