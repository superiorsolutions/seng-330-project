package supsol.toylib.view.formbacker;

//pojo used to back values in an html form
public class CreateBookingBacker {
    private int toyId;
    private int userId;
    private int pickupLibraryId;
    private String pickupDate;
    private String returnDate;
    private boolean maintenance;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getToyId() {
        return toyId;
    }

    public void setToyId(int toyId) {
        this.toyId = toyId;
    }

    public int getPickupLibraryId() {
        return pickupLibraryId;
    }

    public void setPickupLibraryId(int pickupLibraryId) {
        this.pickupLibraryId = pickupLibraryId;
    }

    public String getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public boolean isMaintenance() {
        return maintenance;
    }

    public void setMaintenance(boolean maintenance) {
        this.maintenance = maintenance;
    }
}
