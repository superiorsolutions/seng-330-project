package supsol.toylib.view.formbacker;

//pojo used to back values in an html form
public class EditToyBacker {
    private int toyId;
    private String name;
    private int categoryId;
    private int lastLibraryId;
    private float marketValue;

    public int getToyId() {
        return toyId;
    }

    public void setToyId(int toyId) {
        this.toyId = toyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getLastLibraryId() {
        return lastLibraryId;
    }

    public void setLastLibraryId(int lastLibraryId) {
        this.lastLibraryId = lastLibraryId;
    }

    public float getMarketValue() {
        return marketValue;
    }

    public void setMarketValue(float marketValue) {
        this.marketValue = marketValue;
    }
}
