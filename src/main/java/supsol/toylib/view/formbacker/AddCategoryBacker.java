package supsol.toylib.view.formbacker;

//pojo used to back values in an html form
public class AddCategoryBacker {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
