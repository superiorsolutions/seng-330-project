package supsol.toylib.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import supsol.toylib.entity.Booking;
import supsol.toylib.entity.Library;
import supsol.toylib.entity.Toy;
import supsol.toylib.service.LibraryService;
import supsol.toylib.service.user.UserService;
import supsol.toylib.service.user.UserSession;
import supsol.toylib.view.formbacker.AddLibraryBacker;

import java.util.Map;

/* As a @Controller, this class is responsible for handling requests forwarded in by the servlet container (tomcat)
 * The StaffView handles requests on the /staff url and is mainly responsible for displaying pages about staff menus,
 * operations, and reports.
 * It is also responsible for handling POST requests as a result of form submissions for adding a new library.
 */
//todo: maybe use a spring mvc interceptor to block out non-staff from the /staff path
@Controller
@RequestMapping("/staff")
public class StaffView {
    private static final Logger log = LoggerFactory.getLogger(StaffView.class);

    @Autowired
    private UserSession userSession;
    @Autowired
    private LibraryService libraryService;
    @Autowired
    private UserService userService;

    @RequestMapping("")
    public String showStaffMenu() {
        //make sure the user is a logged in staff member
        if (!(userSession.isLoggedIn() && userSession.getUser().isStaff())) {
            return "redirect:/login";
        }
        return "staff/staffmenu";
    }

    @RequestMapping("/addlibrary")
    public ModelAndView showAddLibrary() {
        if (!(userSession.isLoggedIn() && userSession.getUser().isStaff())) {
            return new ModelAndView("redirect:/login");
        }
        return new ModelAndView("staff/addlibrary", "backer", new AddLibraryBacker());
    }

    @RequestMapping(value = "/addlibrary", method = RequestMethod.POST)
    public ModelAndView doAddLibrary(@ModelAttribute("backer") AddLibraryBacker backer) {
        if (!(userSession.isLoggedIn() && userSession.getUser().isStaff())) {
            return new ModelAndView("redirect:/login");
        }
        Library newLibrary = new Library(backer.getName(), backer.getAddress(), backer.getPhoneNum());
        libraryService.addLibrary(newLibrary);
        return new ModelAndView("redirect:/staff");
    }

    @RequestMapping("/listfees")
    public ModelAndView showUsersWithUnpaidFees() {
        if (!userSession.isStaff()) {
            return new ModelAndView("redirect:/login");
        }
        return new ModelAndView("staff/listfees", "users", userService.getUsersWithUnpaidFees());
    }

    @RequestMapping("/listusers")
    public ModelAndView showAllUsers() {
        if (!userSession.isStaff()) {
            return new ModelAndView("redirect:/login");
        }
        return new ModelAndView("staff/listusers", "users", userService.getAllUsers());
    }

    @RequestMapping("movetoys")
    public ModelAndView showMoveToys() {
        if (!userSession.isStaff()) {
            return new ModelAndView("redirect:/login");
        }
        //get a mapping of toys to their next booking
        Map<Toy, Booking> items = libraryService.getToysToMove();
        //todo: also include libraries in the model so the locations are easier to understand
        return new ModelAndView("staff/movetoys", "items", items);
    }

    @ExceptionHandler
    public ModelAndView showError(Exception e) {
        //if we know the user is a staff member, we can show them a more detailed error
        return new ModelAndView("main/error", "errormsg",
                userSession.isStaff() ? e.getMessage() : "Oops! There was an internal error. Please try again.");
    }

}
