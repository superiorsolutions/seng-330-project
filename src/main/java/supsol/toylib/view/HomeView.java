package supsol.toylib.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import supsol.toylib.entity.Library;
import supsol.toylib.service.LibraryService;
import supsol.toylib.service.user.UserSession;

import java.util.List;

/* As a @Controller, this class is responsible for handling requests forwarded in by the servlet container (tomcat)
 * The HomeView handles requests for the homepage and contact page.
 */
@Controller
public class HomeView {
    private static final Logger log = LoggerFactory.getLogger(HomeView.class);

    @Autowired
    private LibraryService libraryService;
    @Autowired
    private UserSession userSession;

    @RequestMapping("/")
    public ModelAndView showHome() {
        List<Library> libraries = libraryService.getAllLibraries();
        return new ModelAndView("main/home", "libraries", libraries);
    }

    @RequestMapping("/contact")
    public ModelAndView showContact() {
        List<Library> libraries = libraryService.getAllLibraries();
        return new ModelAndView("main/contact", "libraries", libraries);
    }

    @ExceptionHandler
    public ModelAndView showError(Exception e) {
        //if we know the user is a staff member, we can show them a more detailed error
        return new ModelAndView("main/error", "errormsg",
                userSession.isStaff() ? e.getMessage() : "Oops! There was an internal error. Please try again.");
    }
}
