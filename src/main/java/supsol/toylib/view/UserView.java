package supsol.toylib.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import supsol.toylib.entity.Booking;
import supsol.toylib.entity.User;
import supsol.toylib.service.booking.BookingService;
import supsol.toylib.service.user.EmailInUseException;
import supsol.toylib.service.user.UserService;
import supsol.toylib.service.user.UserSession;
import supsol.toylib.view.formbacker.EditUserBacker;
import supsol.toylib.view.formbacker.LoginBacker;
import supsol.toylib.view.formbacker.RegisterBacker;

import java.util.HashMap;
import java.util.List;

/* As a @Controller, this class is responsible for handling requests forwarded in by the servlet container (tomcat)
 * The UserView is responsible for handling requests for login, registration, and edit user pages.
 * It is also responsible for handling POST requests as a result of form submissions.
 */
@Controller
public class UserView {
    private static final Logger log = LoggerFactory.getLogger(UserView.class);

    @Autowired
    private UserSession userSession;
    @Autowired
    private UserService userService;
    @Autowired
    private BookingService bookingService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView showLogin() {
        return new ModelAndView("user/login", "backer", new LoginBacker());
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView doLogin(@ModelAttribute("backer") LoginBacker login) {
        if (!userSession.login(login.getEmail(), login.getPassword())) {
            return new ModelAndView("main/error", "errormsg", "Invalid email/password combination. Please try again.");
        }
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/logout")
    public String doLogout() {
        userSession.logout();
        return "redirect:/";
    }

    @RequestMapping("/register")
    public ModelAndView showRegister() {
        return new ModelAndView("user/register", "backer", new RegisterBacker());
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView doRegister(@ModelAttribute("backer") RegisterBacker registration) {
        //if register while logged in as staff, new user is staff
        boolean newAccountIsStaff = userSession.isLoggedIn() && userSession.getUser().isStaff();

        User newUser = new User(registration.getEmail(), registration.getPassword(), registration.getFullName(),
                registration.getAddress(), registration.getPhoneNum(), newAccountIsStaff);

        try {
            userService.registerUser(newUser);
            return new ModelAndView("user/registerconfirm", "user", newUser);
        } catch (EmailInUseException e) {
            return new ModelAndView("main/error", "errormsg", "That email is already in use. Please try again.");
        }
    }

    @RequestMapping("/edituser")
    public ModelAndView showEditUser(@RequestParam("id") Integer userId) {
        if (!userSession.isLoggedIn()) {
            return new ModelAndView("redirect:/login");
        }
        //if user id given and session user is staff, allow editing another user. otherwise, edit self
        User user;
        if (userSession.getUser().isStaff() && userId != null) {
            user = userService.getUser(userId);
            if (user == null) {
                return new ModelAndView("redirect:/");
            }
        } else {
            user = userSession.getUser();
        }

        //copy the user fields into the form backer to display
        final EditUserBacker backer = new EditUserBacker();
        backer.setUserId(user.getId());
        backer.setEmail(user.getEmail());
        backer.setAddress(user.getAddress());
        backer.setFullName(user.getFullName());
        backer.setPhoneNum(user.getPhoneNum());
        backer.setFees(user.getFees());

        final List<Booking> bookings = bookingService.getBookingsByUser(user.getId());

        //todo: include booking library names and nicer booking status names in model
        return new ModelAndView("user/edituser", new HashMap<String,Object>() {{
            put("backer", backer);
            put("bookings", bookings);
        }});
    }

    @RequestMapping(value = "/edituser", method = RequestMethod.POST)
    public ModelAndView doEditUser(@ModelAttribute("backer") EditUserBacker backer) {
        if (!userSession.isLoggedIn()) {
            return new ModelAndView("redirect:/");
        }
        //make sure people aren't trying to edit another user when they're not supposed to
        if (userSession.getUser().isStaff() || userSession.getUser().getId() == backer.getUserId()) {
            //copy the form backer fields into the user so they can be saved
            User user = userService.getUser(backer.getUserId());
            user.setEmail(backer.getEmail());
            user.setFullName(backer.getFullName());
            user.setAddress(backer.getAddress());
            user.setPhoneNum(backer.getPhoneNum());
            user.setFees(backer.getFees());
            //update the user, show error if fail
            try {
                userService.updateUser(user);
            } catch (EmailInUseException e) {
                return new ModelAndView("main/error", "errormsg", "That email is already in use. Please try again.");
            }

            return new ModelAndView("redirect:/");
        } else {
            log.warn("User " + userSession.getUser().getId() + " tried to edit user " + backer.getUserId());
            return new ModelAndView("redirect:/");
        }
    }

    @ExceptionHandler
    public ModelAndView showError(Exception e) {
        //if we know the user is a staff member, we can show them a more detailed error
        return new ModelAndView("main/error", "errormsg",
                userSession.isStaff() ? e.getMessage() : "Oops! There was an internal error. Please try again.");
    }
}
