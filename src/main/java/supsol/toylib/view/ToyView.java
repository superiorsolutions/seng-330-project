package supsol.toylib.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import supsol.toylib.entity.Category;
import supsol.toylib.entity.Library;
import supsol.toylib.entity.Toy;
import supsol.toylib.service.LibraryService;
import supsol.toylib.service.ToyService;
import supsol.toylib.service.booking.BookingService;
import supsol.toylib.service.user.UserSession;
import supsol.toylib.view.formbacker.AddCategoryBacker;
import supsol.toylib.view.formbacker.AddToyBacker;
import supsol.toylib.view.formbacker.CreateBookingBacker;
import supsol.toylib.view.formbacker.EditToyBacker;

import java.util.HashMap;
import java.util.List;

/* As a @Controller, this class is responsible for handling requests forwarded in by the servlet container (tomcat)
 * The ToyView handles requests on the /toys url and is mainly responsible for displaying pages about toys and categories.
 * It is also responsible for handling POST requests as a result of form submissions.
 */
@Controller
@RequestMapping("/toys")
public class ToyView {
    private static final Logger log = LoggerFactory.getLogger(ToyView.class);

    @Autowired
    private UserSession userSession;
    @Autowired
    private ToyService toyService;
    @Autowired
    private LibraryService libraryService;
    @Autowired
    private BookingService bookingService;

    @RequestMapping("")
    public ModelAndView showCategoryList() {
        List<Category> categories = toyService.getAllCategories();
        return new ModelAndView("toy/categorylist", "categories", categories);
    }

    @RequestMapping("/category")
    public ModelAndView showToysByCategory(@RequestParam("id") int categoryId) {
        final Category category = toyService.getCategory(categoryId);
        if (category == null) {
            return new ModelAndView("error", "errormsg", "That category does not exist.");
        }
        final List<Toy> toys = toyService.getToysByCategory(categoryId);
        return new ModelAndView("toy/toylist", new HashMap<String, Object>() {{
            put("toys", toys);
            put("categoryName", category.getName());
        }});
    }

    @RequestMapping("/addtoy")
    public ModelAndView showAddToy() {
        //make sure the user is a logged in staff member
        if (!(userSession.isLoggedIn() && userSession.getUser().isStaff())) {
            return new ModelAndView("redirect:/");
        }
        final List<Category> categories = toyService.getAllCategories();
        final List<Library> libraries = libraryService.getAllLibraries();
        return new ModelAndView("staff/addtoy", new HashMap<String, Object>() {{
            put("backer", new AddToyBacker());
            put("categories", categories);
            put("libraries", libraries);
        }});
    }

    @RequestMapping(value= "/addtoy", method = RequestMethod.POST)
    public ModelAndView doAddToy(@ModelAttribute("backer") AddToyBacker backer) {
        if (!(userSession.isLoggedIn() && userSession.getUser().isStaff())) {
            return new ModelAndView("redirect:/");
        }
        Toy newToy = new Toy(backer.getName(), backer.getCategoryId(), backer.getLastLibraryId(), backer.getMarketValue());
        toyService.addToy(newToy);
        return new ModelAndView("staff/addtoyconfirm", "toy", newToy);
    }

    @RequestMapping("/view")
    public ModelAndView showToy(@RequestParam("id") int toyId) {
        final Toy toy = toyService.getToy(toyId);
        if (toy == null) {
            return new ModelAndView("error", "errormsg", "That toy does not exist.");
        }

        //backer for create booking
        final CreateBookingBacker backer = new CreateBookingBacker();
        if (userSession.isLoggedIn()) {
            backer.setUserId(userSession.getUser().getId());
        }
        backer.setToyId(toyId);

        return new ModelAndView("toy/viewtoy", new HashMap<String, Object>() {{
            put("booking", bookingService.isToyBooked(toy.getId()));
            put("toy", toy);
            put("library", libraryService.getLibrary(toy.getLastLibraryId()));
            put("backer", backer);
            put("libraries", libraryService.getAllLibraries());
        }});
    }

    @RequestMapping("/edit")
    public ModelAndView editToy(@RequestParam("id") int toyId) {
        if (!(userSession.isLoggedIn() && userSession.getUser().isStaff())) {
            return new ModelAndView("redirect:/");
        }

        final Toy toy = toyService.getToy(toyId);
        if (toy == null) {
            return new ModelAndView("error", "errormsg", "That toy does not exist.");
        }
        final List<Library> libraries = libraryService.getAllLibraries();
        final List<Category> categories = toyService.getAllCategories();

        //copy toy's fields to a form backer object
        final EditToyBacker backer = new EditToyBacker();
        backer.setToyId(toy.getId());
        backer.setName(toy.getName());
        backer.setCategoryId(toy.getCategoryId());
        backer.setLastLibraryId(toy.getLastLibraryId());
        backer.setMarketValue(toy.getMarketValue());

        return new ModelAndView("toy/edittoy", new HashMap<String, Object>() {{
            put("backer", backer);
            put("libraries", libraries);
            put("categories", categories);
        }});
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String doEditToy(@ModelAttribute("backer") EditToyBacker backer) {
        if (!(userSession.isLoggedIn() && userSession.getUser().isStaff())) {
            return "redirect:/";
        }

        //copy backer's fields into the toy to save
        Toy toy = toyService.getToy(backer.getToyId());
        toy.setName(backer.getName());
        toy.setCategoryId(backer.getCategoryId());
        toy.setLastLibraryId(backer.getLastLibraryId());
        toy.setMarketValue(backer.getMarketValue());
        toyService.updateToy(toy);

        return "redirect:/toys/view?id=" + toy.getId();
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ModelAndView doDeleteToy(@ModelAttribute("backer") EditToyBacker backer) {
        if (!(userSession.isLoggedIn() && userSession.getUser().isStaff())) {
            return new ModelAndView("redirect:/");
        }

        if (!toyService.removeToy(backer.getToyId())) {
            return new ModelAndView("main/error", "errormsg", "Could not delete toy " + backer.getToyId() + " -- toy not found");
        }
        return new ModelAndView("redirect:/toys/category?id=" + backer.getCategoryId());
    }

    @RequestMapping("/addcategory")
    public ModelAndView showAddCategory() {
        if (!(userSession.isLoggedIn() && userSession.getUser().isStaff())) {
            return new ModelAndView("redirect:/");
        }
        return new ModelAndView("staff/addcategory", "backer", new AddCategoryBacker());
    }

    @RequestMapping(value = "/addcategory", method = RequestMethod.POST)
    public String doAddCategory(@ModelAttribute("backer") AddCategoryBacker backer) {
        if (!(userSession.isLoggedIn() && userSession.getUser().isStaff())) {
            return"redirect:/";
        }
        Category category = new Category(backer.getName());
        toyService.addCategory(category);
        return "redirect:/staff";
    }

    @ExceptionHandler
    public ModelAndView showError(Exception e) {
        //if we know the user is a staff member, we can show them a more detailed error
        return new ModelAndView("main/error", "errormsg",
                userSession.isStaff() ? e.getMessage() : "Oops! There was an internal error. Please try again.");
    }
}
