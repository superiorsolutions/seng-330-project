package supsol.toylib.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.spring3.SpringTemplateEngine;
import org.thymeleaf.spring3.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import javax.annotation.PreDestroy;
import javax.sql.DataSource;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;

/* This class is used by Spring to configure our application. It defines the sources of beans required by various
 * parts of the application, as well as where to find more beans annotated with sub-annotations of @Component.
 * All beans are implicitly singletons.
 */
@Configuration
@EnableTransactionManagement
@EnableWebMvc
@PropertySource("classpath:/supsol/toylib/config/email.properties")
@ComponentScan(basePackages = {"supsol/toylib"}, scopedProxy = ScopedProxyMode.TARGET_CLASS)
public class AppConfig extends WebMvcConfigurerAdapter {
    private static final Logger log = LoggerFactory.getLogger(AppConfig.class);

    //todo: try fix for hot deploy: http://cdivilly.wordpress.com/2012/04/23/permgen-memory-leak/

    @Autowired
    Environment env;

    //the source of our database connection
    @Bean
    public DataSource dataSource() {
        //we could alternatively create a data source connecting to an external database rather than an in-memory one
        return new EmbeddedDatabaseBuilder().addScript("supsol/toylib/config/bootstrap.sql").setType(EmbeddedDatabaseType.H2).build();
    }

    //a bean used to facilitate sending emails from the application
    @Bean
    public MailSender mailSender() {
        JavaMailSenderImpl sender = new JavaMailSenderImpl();
        sender.setHost(env.getProperty("email.host"));
        sender.setPassword(env.getProperty("email.password"));
        sender.setUsername(env.getProperty("email.username"));
        sender.setPort(Integer.parseInt(env.getProperty("email.port")));
        Properties javaMailPropertes = new Properties();
        javaMailPropertes.put("mail.smtp.auth", true);
        javaMailPropertes.put("mail.smtp.starttls.enable", true);
        sender.setJavaMailProperties(javaMailPropertes);
        return sender;
    }

    //used in every DAO class to perform queries and updates on the database
    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    //allows methods to be @Transactional, meaning all database operations in it are an atomic unit
    @Bean
    public PlatformTransactionManager txManager() {
        return new DataSourceTransactionManager(dataSource());
    }

    @Bean
    public ThymeleafViewResolver viewResolver() {
        ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
        viewResolver.setTemplateEngine(templateEngine());
        viewResolver.setOrder(1);
        return viewResolver;
    }

    @Bean
    public SpringTemplateEngine templateEngine() {
        SpringTemplateEngine engine = new SpringTemplateEngine();
        engine.setTemplateResolver(templateResolver());
        return engine;
    }

    //used by Spring MVC to find corresponding html files when we specify their names as views
    @Bean
    public ServletContextTemplateResolver templateResolver() {
        ServletContextTemplateResolver resolver = new ServletContextTemplateResolver();
        resolver.setPrefix("/views/");
        resolver.setSuffix(".html");
        resolver.setTemplateMode("HTML5");
        return resolver;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/**");
    }

    //runs during shutdown of the application
    @PreDestroy
    public void contextDestroyed() {
        //deregister drivers (like H2) if we are not storing them in tomcat's lib dir
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            try {
                DriverManager.deregisterDriver(driver);
                log.info("Successfully deregistered driver: " + driver);
            } catch (SQLException e) {
                log.error("Failed to deregister driver: " + driver, e);
            }
        }
    }
}
