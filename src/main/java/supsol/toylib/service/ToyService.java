package supsol.toylib.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import supsol.toylib.entity.Category;
import supsol.toylib.entity.CategoryGateway;
import supsol.toylib.entity.Toy;
import supsol.toylib.entity.ToyGateway;

import java.util.List;

//This service is responsible for the creation, modification, and retrieval of toys and their categories.
@Service
public class ToyService {
    private static Logger log = LoggerFactory.getLogger(ToyService.class);

    @Autowired
    private ToyGateway toyDao;
    @Autowired
    private CategoryGateway categoryDao;

    public void addCategory(Category category) {
        try {
            log.info("Adding new category " + category.getName());
            categoryDao.save(category);
        } catch (DataAccessException e) {
            log.error("Failed to add category " + category.getName(), e);
            throw e;
        }
    }

    public List<Category> getAllCategories() {
        try {
            return categoryDao.getAll();
        } catch (DataAccessException e) {
            log.error("Failed to get all categories from database", e);
            throw e;
        }
    }

    public Category getCategory(int categoryId) {
        try {
            return categoryDao.get(categoryId);
        } catch (DataAccessException e) {
            log.error("Failed to get category " + categoryId + " from database", e);
            throw e;
        }
    }

    public List<Toy> getToysByCategory(int categoryId) {
        try {
            return toyDao.getToysByCategory(categoryId);
        } catch (DataAccessException e) {
            log.error("Failed to get toys from database by category " + categoryId, e);
            throw e;
        }
    }

    public void addToy(Toy toy) {
        try {
            log.info("Adding new toy to the system: " + toy.getName());
            toyDao.save(toy);
        } catch (DataAccessException e) {
            log.error("Failed to add new toy to database " + toy.getName(), e);
            throw e;
        }
    }

    public void updateToy(Toy toy) {
        try {
            log.info("Updating toy : " + toy.getId());
            toyDao.save(toy);
        } catch (DataAccessException e) {
            log.error("Failed to update toy " + toy.getId(), e);
            throw e;
        }
    }

    public Toy getToy(int toyId) {
        try {
            return toyDao.get(toyId);
        } catch (DataAccessException e) {
            log.error("Failed to get toy from database by id " + toyId, e);
            throw e;
        }
    }

    public boolean removeToy(int toyId) {
        try {
            log.info("Removing toy " + toyId + " and associated bookings from the system");
            if (!toyDao.delete(toyId)) {
                log.error("Could not find toy " + toyId);
                return false;
            }
        } catch (DataAccessException e) {
            log.error("Failed to delete toy " + toyId, e);
            throw e;
        }
        return true;
    }
}
