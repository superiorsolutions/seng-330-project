package supsol.toylib.service.booking;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import supsol.toylib.entity.Booking;
import supsol.toylib.entity.BookingGateway;
import supsol.toylib.entity.Toy;
import supsol.toylib.service.ToyService;
import supsol.toylib.service.user.UserService;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

//Responsible for creating, updating, and retrieving bookings. The service must ensure business rules are followed.
@Service("bookingService")
public class BookingService {
    private static final Logger log = LoggerFactory.getLogger(BookingService.class);

    private static final int LATE_RETURNS_FOR_SUSPENSION = 3;
    private static final int SUSPENSION_PERIOD_DAYS = 365;
    private static final float BOOKING_FEE = 1.0F;
    private static final int MAX_TOY_TOTAL_DAYS_PER_YEAR = 25;

    @Autowired
    private BookingGateway bookingDao;
    @Autowired
    private UserService userService;
    @Autowired
    private ToyService toyService;

    @Transactional(rollbackFor = Exception.class)
    public void createBooking(Booking booking)
            throws OverdueBookingsException, AccountSuspendedException, BookingLimitExceededException, CannotMoveToyException {
        log.info("Creating booking for toy " + booking.getToyId() + " by user " + booking.getUserId());
        try {
            Date today = DateUtils.truncate(new Date(), Calendar.DATE);
            Date returnDate = DateUtils.truncate(booking.getReturnDate(), Calendar.DATE);
            Date pickupDate = DateUtils.truncate(booking.getPickupDate(), Calendar.DATE);

            //does this user have overdue bookings that need to be returned?
            List<Booking> overdueBookings = bookingDao.getOverdueBookingsByUser(booking.getUserId());
            if (overdueBookings.size() > 0) {
                log.info("Could not create booking for user " + booking.getUserId() + " because they have overdue bookings");
                throw new OverdueBookingsException(overdueBookings);
            }

            //is this users account suspended from creating bookings?
            List<Booking> lateReturns = bookingDao.getLateReturnsByUser(booking.getUserId(), SUSPENSION_PERIOD_DAYS);
            if (lateReturns.size() >= LATE_RETURNS_FOR_SUSPENSION) {
                log.info("Could not create booking for user " + booking.getUserId() + " because their account is suspended");
                throw new AccountSuspendedException(lateReturns, SUSPENSION_PERIOD_DAYS);
            }

            //has this user already booked this toy too much this year?
            int daysBooked = bookingDao.getTotalDaysBookedSince(booking.getUserId(), booking.getToyId(), DateUtils.addYears(new Date(), -1));
            long bookingDuration = TimeUnit.MILLISECONDS.toDays(returnDate.getTime() - pickupDate.getTime());
            if (bookingDuration > MAX_TOY_TOTAL_DAYS_PER_YEAR || daysBooked >= MAX_TOY_TOTAL_DAYS_PER_YEAR) {
                throw new BookingLimitExceededException(booking.getToyId(), MAX_TOY_TOTAL_DAYS_PER_YEAR);
            }

            //are they trying to move the toy on the same day as booking it?
            Toy toy = toyService.getToy(booking.getToyId());
            if (toy.getLastLibraryId() != booking.getPickupLibraryId() && pickupDate.equals(today)) {
                throw new CannotMoveToyException(1);
            }

            //save the booking
            bookingDao.save(booking);
            userService.emailStaff("Booking created: " + booking.getId(), "A new booking was created by user " + booking.getUserId());
        } catch (DataAccessException e) {
            log.error("Failed to create booking", e);
            throw e;
        }
    }

    //creates a booking without any business checks that is automatically approved
    public void createMaintenanceBooking(Booking booking) {
        log.info("Creating maintenance booking for toy " + booking.getToyId());
        try {
            booking.setStatus(Booking.BookingStatus.APPROVED);
            bookingDao.save(booking);
        } catch (DataAccessException e) {
            log.error("Failed to book toy " + booking.getToyId() + " for maintenenace", e);
            throw e;
        }
    }

    public Booking getBooking(int bookingId) {
        try {
            return bookingDao.get(bookingId);
        } catch (DataAccessException e) {
            log.error("Failed to get booking from database with id " + bookingId);
            throw e;
        }
    }

    public List<Booking> getBookingsByUser(int userId) {
        try {
            return bookingDao.getBookingsByUser(userId);
        } catch (DataAccessException e) {
            log.error("Failed to get bookings from database by user id " + userId);
            throw e;
        }
    }

    public void updateBooking(Booking booking) {
        log.info("Updating booking " + booking.getId());
        try {
            //handle status changes
            Booking oldBooking = bookingDao.get(booking.getId());
            if (oldBooking.getStatus() != booking.getStatus()) {
                switch (booking.getStatus()) {
                    case APPROVED:
                        //charge the user and send them an email
                        userService.emailUser(booking.getUserId(), "Your booking has been approved", "Booking ID " + booking.getId());
                        userService.chargeUser(booking.getUserId(), BOOKING_FEE);
                        break;
                    case CANCELLED:
                        //if the booking wasn't yet picked up but was cancelled, refund their fee
                        if (oldBooking.getStatus() == Booking.BookingStatus.APPROVED) {
                            userService.chargeUser(booking.getUserId(), -BOOKING_FEE);
                        }
                        userService.emailUser(booking.getUserId(), "Your booking has been cancelled", "Booking ID " + booking.getId());
                        break;
                }
            }

            bookingDao.save(booking);
        } catch (DataAccessException e) {
            log.error("Failed to update booking " + booking.getId(), e);
            throw e;
        }
    }

    //used by the tentative bookings view that staff use to schedule bookings
    public List<Booking> getTentativeBookings() {
        log.info("Getting all tentative bookings");
        try {
            return bookingDao.getTentativeBookings();
        } catch (DataAccessException e) {
            log.error("Failed to get tentative bookings", e);
            throw e;
        }
    }

    //used in the interface to display fee information to the user
    public float getBookingFee(boolean staffDiscount) {
        return BOOKING_FEE * (staffDiscount ? UserService.STAFF_FEE_ADJUSTMENT : 1.0F);
    }

    public Booking isToyBooked(int toyId) {
        try {
            return bookingDao.isToyBooked(toyId);
        } catch (DataAccessException e) {
            log.error("Failed to determine if toy " + toyId + " is booked", e);
            throw e;
        }
    }

    //get approved bookings in the near future, used by the staff view to determine which toys need to be moved
    public List<Booking> getFutureApprovedBookings(int days) {
        try {
            return bookingDao.getFutureApprovedBookings(days);
        } catch (DataAccessException e) {
            log.error("Failed to get future bookings", e);
            throw e;
        }
    }
}
