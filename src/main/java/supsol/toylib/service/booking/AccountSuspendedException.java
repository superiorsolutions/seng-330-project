package supsol.toylib.service.booking;

import supsol.toylib.entity.Booking;

import java.util.List;

//thrown during booking creation when it is discovered that the user has a suspended account
public class AccountSuspendedException extends Exception {
    private List<Booking> lateReturns;
    private int suspensionPeriod;

    public AccountSuspendedException(List<Booking> lateReturns, int suspensionPeriod) {
        this.lateReturns = lateReturns;
        this.suspensionPeriod = suspensionPeriod;
    }

    public List<Booking> getLateReturns() {
        return lateReturns;
    }

    public int getSuspensionPeriod() {
        return suspensionPeriod;
    }
}
