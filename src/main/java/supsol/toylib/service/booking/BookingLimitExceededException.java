package supsol.toylib.service.booking;

//thrown during booking creation when it is discovered that the user has booked a toy for a maximum yearly amount
public class BookingLimitExceededException extends Exception {
    private int toyId;
    private int limit;

    public BookingLimitExceededException(int toyId, int limit) {
        this.toyId = toyId;
        this.limit = limit;
    }

    public int getToyId() {
        return toyId;
    }

    public int getLimit() {
        return limit;
    }
}
