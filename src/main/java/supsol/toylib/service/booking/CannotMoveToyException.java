package supsol.toylib.service.booking;

//thrown during booking creation when a toy cannot be moved to a different library quickly enough for pickup
public class CannotMoveToyException extends Exception {
    private int requiredDays;

    public CannotMoveToyException(int requiredDays) {
        this.requiredDays = requiredDays;
    }

    public int getRequiredDays() {
        return requiredDays;
    }
}
