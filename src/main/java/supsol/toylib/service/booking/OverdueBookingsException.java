package supsol.toylib.service.booking;

import supsol.toylib.entity.Booking;

import java.util.List;

//thrown during booking creation when it is discovered that the user has unreturned toys preventing the creation of further bookings
public class OverdueBookingsException extends Exception {
    private List<Booking> overdueBookings;

    public OverdueBookingsException(List<Booking> overdueBookings) {
        this.overdueBookings = overdueBookings;
    }

    public List<Booking> getOverdueBookings() {
        return overdueBookings;
    }
}
