package supsol.toylib.service.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import supsol.toylib.entity.User;
import supsol.toylib.entity.UserGateway;

import java.util.ArrayList;
import java.util.List;

/* This service is responsible for the creation, modification, and retrieval of users, as well as providing
 * an interface to charging users and sending emails.
 */
@Service("userService")
public class UserService {
    private static final Logger log = LoggerFactory.getLogger(UserService.class);

    private static final float REGISTRATION_FEE = 25.00F;
    public static final float STAFF_FEE_ADJUSTMENT = 0.75F;
    private static final String STAFF_EMAIL_ADDRESS = "toysareyours@gmail.com";

    @Autowired
    private UserGateway userDao;
    @Autowired
    private MailSender mailSender;

    //lists of listeners
    private List<UserUpdateListener> userUpdateListeners = new ArrayList<UserUpdateListener>();
    private List<RegistrationListener> registrationListeners = new ArrayList<RegistrationListener>();

    public void addUserUpdateListener(UserUpdateListener listener) {
        userUpdateListeners.add(listener);
    }

    public void addRegistrationListener(RegistrationListener listener) {
        registrationListeners.add(listener);
    }

    @Transactional(rollbackFor = Exception.class)
    public void registerUser(User user) throws EmailInUseException {
        try {
            userDao.save(user);
            for (RegistrationListener listener : registrationListeners) {
                listener.userRegistered(user);
            }
            chargeUser(user.getId(), REGISTRATION_FEE);
            log.info("Registered new user " + user.getEmail() + " with id " + user.getId());
        } catch (DataIntegrityViolationException e) {
            //since the only unique field in the table is email, we can throw this exception
            log.debug("Tried to register user with an email that already exists", e);
            throw new EmailInUseException();
        } catch (DataAccessException e) {
            log.error("Failed to register new user " + user.getEmail(), e);
            throw e;
        }
    }

    public void updateUser(User user) throws EmailInUseException {
        try {
            userDao.save(user);
            log.info("Updated user " + user.getId());
            for (UserUpdateListener listener : userUpdateListeners) {
                listener.userUpdated(user);
            }
        } catch (DataIntegrityViolationException e) {
            log.debug("Tried to set user email that already exists", e);
            throw new EmailInUseException();
        } catch (DataAccessException e) {
            log.error("Failed to update user " + user.getEmail(), e);
            throw e;
        }
    }

    public User getUser(int userId) {
        try {
            return userDao.get(userId);
        } catch (DataAccessException e) {
            log.error("Failed to get user from database by id " + userId, e);
            throw e;
        }
    }

    public User getUserByLogin(String email, String passwordHash) {
        try {
            return userDao.getUserByLogin(email, passwordHash);
        } catch (DataAccessException e) {
            log.error("Failed to get user from database by login " + email, e);
            throw e;
        }
    }

    public List<User> getAllUsers() {
        try {
            return userDao.getAll();
        } catch (DataAccessException e) {
            log.error("Failed to get all users", e);
            throw e;
        }
    }

    public List<User> getUsersWithUnpaidFees() {
        try {
            return userDao.getUsersWithUnpaidFees();
        } catch (DataAccessException e) {
            log.error("Failed to get unpaid users from database", e);
            throw e;
        }
    }

    //sends emails to parents
    public void emailUser(int userId, String subject, String emailText) {
        User user = userDao.get(userId);
        sendEmail(STAFF_EMAIL_ADDRESS, user.getEmail(), subject, emailText);
    }

    //sends emails to the staff email address
    public void emailStaff(String subject, String emailText) {
        sendEmail(STAFF_EMAIL_ADDRESS, STAFF_EMAIL_ADDRESS, subject, emailText);
    }

    private void sendEmail(final String from, final String to, final  String subject, final String message) {
        log.debug("Sending email to " + to + ": " + subject);
        try {
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setFrom(from);
            mailMessage.setTo(to);
            mailMessage.setSubject(subject);
            mailMessage.setText(message);
            mailSender.send(mailMessage);
        } catch (MailException e) {
            log.error("Failed to send email", e);
            throw e;
        }
    }

    public void chargeUser(int userId, float amount) {
        User user = userDao.get(userId);
        if (user.isStaff()) {
            amount *= STAFF_FEE_ADJUSTMENT;
        }
        user.setFees(user.getFees() + amount);

        //call updateUser rather than the DAO directly so our update listeners are notified
        try {
            updateUser(user);
        } catch (EmailInUseException e) {}
    }

    public float getRegistrationFee(boolean staffDiscount) {
        return REGISTRATION_FEE * (staffDiscount ? STAFF_FEE_ADJUSTMENT : 1.0F);
    }

    //used by the contact page
    public String getStaffEmailAddress() {
        return STAFF_EMAIL_ADDRESS;
    }
}
