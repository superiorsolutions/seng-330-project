package supsol.toylib.service.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import supsol.toylib.entity.User;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.servlet.http.HttpSession;

/* This service layer object is not technically a singleton, but can be treated as one on a per-session basis. It has a
 * session scope so that a thread handling a web request only has access to one of these objects, and only to the
 * one associated with the HTTP session.
 *
 * The main responsibility of this class is to retain login information and thereby enforce permissions across the site.
 *
 * If we had time to implement a cart system for bookings, we could do it here.
 * Tomcat may complain about this class not being serializable between restarts, but can ignore (can't serialize userDao).
 * Maybe this class shouldn't know about the database though? Could try passing this to UserService to proxy the work */
@Service("userSession")
@Scope("session")
public class UserSession implements UserUpdateListener, RegistrationListener {
    private static final Logger log = LoggerFactory.getLogger(UserSession.class);

    //the user will be logged out after 30 minutes
    private static final int SESSION_TIMEOUT_MINUTES = 30;

    @Autowired
    private HttpSession session;
    @Autowired
    private UserService userService;
    private User user;
    private String sessionId;

    @PostConstruct
    public void sessionStart() {
        session.setMaxInactiveInterval(60 * SESSION_TIMEOUT_MINUTES);
        sessionId = session.getId();

        userService.addRegistrationListener(this);
        userService.addUserUpdateListener(this);

        log.debug("Session started: " + sessionId);
    }

    //allows views and services to login a user, such as upon registration or when the user logs in on the site
    public boolean login(String email, String password) {
        try {
            User user = userService.getUserByLogin(email, User.hashPassword(password));
            if (user == null) {
                log.debug("Could not find user with email " + email);
                return false;
            }
            log.debug("User " + email + " logged in with session id " + session.getId());
            this.user = user;
            return true;
        } catch (DataAccessException e) {
            log.error("Failed to login user", e);
            throw e;
        }
    }

    //clears the user from the session
    public void logout() {
        log.debug("Session " + session.getId() + " is logging out");
        user = null;
    }

    //listens to events where a new user has been registered, in which we want to become that user
    @Override
    public void userRegistered(User user) {
        this.user = user;
    }

    /* listens to events where a user has been updated, and if that user is the currently logged in user, update
     * the state of this object to remain consistent
     */
    @Override
    public void userUpdated(User user) {
        if (user != null && getUser().getId() == user.getId()) {
            this.user = user;
        }
    }

    public User getUser() {
        return user;
    }

    public boolean isLoggedIn() {
        return user != null;
    }

    public boolean isStaff() {
        return user != null && user.isStaff();
    }

    public HttpSession getHttpSession() {
        return session;
    }

    @PreDestroy
    private void sessionClose() {
        /* We can't use session in here or else we get an IllegalStateException because tomcat
         * has destroyed the session object. Thus we use the sessionId saved at creation instead */
        log.debug("Session closed: " + sessionId);
    }
}
