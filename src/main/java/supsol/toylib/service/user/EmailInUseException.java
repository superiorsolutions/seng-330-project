package supsol.toylib.service.user;

//thrown during registration when it is found the user has already registered with that email address
public class EmailInUseException extends Exception {
}
