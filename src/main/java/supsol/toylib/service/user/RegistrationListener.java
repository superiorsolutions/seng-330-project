package supsol.toylib.service.user;

import supsol.toylib.entity.User;

//represents a class that subscribes to the registration of new users
public interface RegistrationListener {
    public void userRegistered(User user);
}
