package supsol.toylib.service.user;

import supsol.toylib.entity.User;

//represents a class that subscribes to changes made to users
public interface UserUpdateListener {
    public void userUpdated(User user);
}
