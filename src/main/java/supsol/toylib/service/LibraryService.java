package supsol.toylib.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import supsol.toylib.entity.Booking;
import supsol.toylib.entity.Library;
import supsol.toylib.entity.LibraryGateway;
import supsol.toylib.entity.Toy;
import supsol.toylib.service.booking.BookingService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* This service is responsible for business and library-related tasks, such as adding locations and
 * generating reports.
 */
@Service
public class LibraryService {
    private static final Logger log = LoggerFactory.getLogger(LibraryService.class);

    private static final int MOVE_TOY_LIST_DAYS_AHEAD = 14;

    @Autowired
    private LibraryGateway libraryDao;
    @Autowired
    private BookingService bookingService;
    @Autowired
    private ToyService toyService;

    public List<Library> getAllLibraries() {
        try {
            return libraryDao.getAll();
        } catch (DataAccessException e) {
            log.error("Failed to get all libraries from database", e);
            throw e;
        }
    }

    public void addLibrary(Library newLibrary) {
        try {
            log.info("Adding new library to the system: " + newLibrary.getName());
            libraryDao.save(newLibrary);
        } catch (DataAccessException e) {
            log.error("Failed to add library " + newLibrary.getName(), e);
            throw e;
        }
    }

    public Library getLibrary(int libraryId) {
        try {
            return libraryDao.get(libraryId);
        } catch (DataAccessException e) {
            log.error("Failed to get a library from database by id " + libraryId, e);
            throw e;
        }
    }

    //gets a list of toys that need to be moved to another library location for an upcoming booking pickup
    @Transactional
    public Map<Toy, Booking> getToysToMove() {
        try {
            Map<Toy, Booking> moveList = new HashMap<Toy, Booking>();

            //for each approved booking in the future, check if it's toy is in the wrong place.
            List<Booking> futureBookings = bookingService.getFutureApprovedBookings(MOVE_TOY_LIST_DAYS_AHEAD);
            for (Booking booking : futureBookings) {
                Toy toy = toyService.getToy(booking.getToyId());
                //if this toy needs to be moved
                if (toy.getLastLibraryId() != booking.getPickupLibraryId()) {
                    //replaces a mapping for the same toy if it's already there. futureBookings ordered latest -> earliest
                    moveList.put(toy, booking);
                }
            }
            return moveList;
        } catch (DataAccessException e) {
            log.error("Failed to get toys to move list", e);
            throw e;
        }
    }
}
