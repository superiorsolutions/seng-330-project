package supsol.toylib.entity;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class User implements PersistentEntity {
    private int id;
    private String email;
    private String passwordHash;
    private String fullName;
    private String address;
    private String phoneNum;
    private boolean staff;
    private float fees;
    //todo: use long instead of float for currencies in this application. float simpler for now

    //public constructor used by views passing the entity to the service layer
    public User(String email, String password, String fullName, String address, String phoneNum, boolean staff) {
        this.id = -1;
        this.email = email;
        this.passwordHash = hashPassword(password);
        this.fullName = fullName;
        this.address = address;
        this.phoneNum = phoneNum;
        this.staff = staff;
    }

    //private constructor that includes the ID, used by this entity's rowmapper
    private User(int id, String email, String passwordHash, String fullName, String address, String phoneNum,
                 boolean staff, float fees) {
        this.id = id;
        this.email = email;
        this.passwordHash = passwordHash;
        this.fullName = fullName;
        this.address = address;
        this.phoneNum = phoneNum;
        this.staff = staff;
        this.fees = fees;
    }

    @Override
    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean isSaved() {
        return id != -1;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.passwordHash = hashPassword(password);
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public boolean isStaff() {
        return staff;
    }

    public void setStaff(boolean staff) {
        this.staff = staff;
    }

    public float getFees() {
        return fees;
    }

    public void setFees(float fees) {
        this.fees = fees;
    }

    //using a simple password hashing scheme for now--no salt and 1 iteration
    public static String hashPassword(String plaintext) {
        return DigestUtils.sha256Hex(plaintext);
    }

    //maps a row from the database to this entity
    public static class UserRowMapper implements RowMapper<User> {
        @Override
        public User mapRow(ResultSet resultSet, int i) throws SQLException {
            return new User(
                    resultSet.getInt("user_id"),
                    resultSet.getString("email"),
                    resultSet.getString("password_hash"),
                    resultSet.getString("full_name"),
                    resultSet.getString("address"),
                    resultSet.getString("phone_num"),
                    resultSet.getBoolean("staff"),
                    resultSet.getFloat("fees"));
        }
    }

    @Repository
    public static class UserDao implements UserGateway {
        private static final Logger log = LoggerFactory.getLogger(UserDao.class);

        @Autowired
        private JdbcTemplate jdbcTemplate;

        @Override
        public void save(final User entity) {
            if (entity.isSaved()) {
                log.debug("Updating user " + entity.getId());
                jdbcTemplate.update("update lib_user set email = ?, password_hash = ?, full_name = ?, address = ?, " +
                        "phone_num = ?, staff = ?, fees = ? where user_id = ?", entity.getEmail(), entity.getPasswordHash(),
                        entity.getFullName(), entity.getAddress(), entity.getPhoneNum(), entity.isStaff(), entity.getFees(),
                        entity.getId());
            } else {
                log.debug("Inserting user " + entity.getEmail());
                SimpleJdbcInsert insert = new SimpleJdbcInsert(jdbcTemplate)
                        .withTableName("lib_user")
                        .usingGeneratedKeyColumns("user_id");
                int id = insert.executeAndReturnKey(new HashMap<String, Object>() {{
                    put("email", entity.getEmail());
                    put("password_hash", entity.getPasswordHash());
                    put("full_name", entity.getFullName());
                    put("address", entity.getAddress());
                    put("phone_num", entity.getPhoneNum());
                    put("staff", entity.isStaff());
                    put("fees", entity.getFees());
                }}).intValue();
                entity.setId(id);
            }
        }

        @Override
        public List<User> getAll() {
            log.debug("Getting all users");
            return jdbcTemplate.query("select * from lib_user", new UserRowMapper());
        }

        @Override
        public User get(int id) {
            log.debug("Getting user by id " + id);
            List<User> users = jdbcTemplate.query("select * from lib_user where user_id = ?", new UserRowMapper(), id);
            if (users.size() != 1) {
                log.warn("Could not find user " + id);
                return null;
            }
            return users.get(0);
        }

        @Override
        public boolean delete(int id) {
            log.debug("Deleting user by id " + id);
            return jdbcTemplate.update("delete from lib_user where user_id = ?", id) == 1;
        }

        //used when logging in a user
        @Override
        public User getUserByLogin(String email, String passwordHash) {
            log.debug("Getting user by login " + email);
            List<User> users = jdbcTemplate.query("select * from lib_user where email = ? and password_hash = ?",
                    new User.UserRowMapper(), email, passwordHash);
            if (users.size() != 1) {
                log.debug("Could not find email/pass combo for " + email);
                return null;
            }
            return users.get(0);
        }

        @Override
        public List<User> getUsersWithUnpaidFees() {
            log.debug("Getting users with unpaid fees");
            return jdbcTemplate.query("select * from lib_user where fees > 0 order by fees desc", new UserRowMapper());
        }
    }
}
