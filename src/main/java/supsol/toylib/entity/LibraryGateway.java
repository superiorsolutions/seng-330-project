package supsol.toylib.entity;

/* reference: http://martinfowler.com/eaaCatalog/tableDataGateway.html
 * This interface requires each DAO class to implement CRUD operations for persistent entities.
 * This interface is also extended by Gateway interfaces more specific to their particular entity.
 */
public interface LibraryGateway extends EntityGateway<Library> {

}
