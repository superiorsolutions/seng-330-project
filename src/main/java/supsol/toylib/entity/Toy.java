package supsol.toylib.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class Toy implements PersistentEntity {
    private int id;
    private String name;
    private int categoryId;
    private int lastLibraryId;
    private float marketValue;

    //public constructor used by views passing the entity to the service layer
    public Toy(String name, int categoryId, int lastLibraryId, float marketValue) {
        this.id = -1;
        this.name = name;
        this.categoryId = categoryId;
        this.lastLibraryId = lastLibraryId;
        this.marketValue = marketValue;
    }

    //private constructor that includes the ID, used by this entity's rowmapper
    private Toy(int id, String name, int category_id, int lastLibraryId, float marketValue) {
        this.id = id;
        this.name = name;
        this.categoryId = category_id;
        this.lastLibraryId = lastLibraryId;
        this.marketValue = marketValue;
    }

    @Override
    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean isSaved() {
        return id != -1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getLastLibraryId() {
        return lastLibraryId;
    }

    public void setLastLibraryId(int lastLibraryId) {
        this.lastLibraryId = lastLibraryId;
    }

    public float getMarketValue() {
        return marketValue;
    }

    public void setMarketValue(float marketValue) {
        this.marketValue = marketValue;
    }

    //maps a row from the database to this entity
    public static class ToyRowMapper implements RowMapper<Toy> {
        @Override
        public Toy mapRow(ResultSet resultSet, int i) throws SQLException {
            return new Toy(
                    resultSet.getInt("toy_id"),
                    resultSet.getString("name"),
                    resultSet.getInt("category_id"),
                    resultSet.getInt("last_library_id"),
                    resultSet.getFloat("market_value"));
        }
    }

    @Repository
    public static class ToyDao implements ToyGateway {
        private static final Logger log = LoggerFactory.getLogger(ToyDao.class);

        @Autowired
        private JdbcTemplate jdbcTemplate;

        @Override
        public void save(final Toy entity) {
            if (entity.isSaved()) {
                log.debug("Updating toy " + entity.getId());
                jdbcTemplate.update("update toy set name = ?, category_id = ?, last_library_id = ?, " +
                        "market_value = ? where toy_id = ?", entity.getName(), entity.getCategoryId(),
                        entity.getLastLibraryId(), entity.getMarketValue(), entity.getId());
            } else {
                log.debug("Inserting toy " + entity.getName());
                SimpleJdbcInsert insert = new SimpleJdbcInsert(jdbcTemplate)
                        .withTableName("toy")
                        .usingGeneratedKeyColumns("toy_id");
                int id = insert.executeAndReturnKey(new HashMap<String, Object>() {{
                    put("name", entity.getName());
                    put("category_id", entity.getCategoryId());
                    put("last_library_id", entity.getLastLibraryId());
                    put("market_value", entity.getMarketValue());
                }}).intValue();
                entity.setId(id);
            }
        }

        @Override
        public List<Toy> getAll() {
            log.debug("Getting all toys");
            return jdbcTemplate.query("select * from toy", new ToyRowMapper());
        }

        @Override
        public Toy get(int id) {
            log.debug("Getting toy by id " + id);
            List<Toy> toys = jdbcTemplate.query("select * from toy where toy_id = ?", new ToyRowMapper(), id);
            if (toys.size() != 1) {
                log.warn("Could not find toy " + id);
                return null;
            }
            return toys.get(0);
        }

        @Override
        public boolean delete(int id) {
            log.debug("Deleting toy by id " + id);
            return jdbcTemplate.update("delete from toy where toy_id = ?", id) == 1;
        }

        @Override
        public List<Toy> getToysByCategory(int categoryId) {
            log.debug("Getting toy by category " + categoryId);
            return jdbcTemplate.query("select * from toy where category_id = ?", new ToyRowMapper(), categoryId);
        }
    }
}
