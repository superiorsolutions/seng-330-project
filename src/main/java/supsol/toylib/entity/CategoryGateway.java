package supsol.toylib.entity;

/* while the CategoryDAO needs no more extra behaviours now, this interface exists to maintain provide a skeleton for
 * future additions and keep consistency with other entities. moreover, it helps us avoid the need for @Qualifiers on
 * Spring-injected beans of type EntityGateway due to java's runtime type-erasure.
 */
public interface CategoryGateway extends EntityGateway<Category> {

}
