package supsol.toylib.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class Library implements PersistentEntity {
    private int id;
    private String name;
    private String address;
    private String phoneNum;

    //private constructor that includes the ID, used by this entity's rowmapper
    private Library(int id, String name, String address, String phoneNum) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phoneNum = phoneNum;
    }

    //public constructor used by views passing the entity to the service layer
    public Library(String name, String address, String phoneNum) {
        this.id = -1;
        this.name = name;
        this.address = address;
        this.phoneNum = phoneNum;
    }

    @Override
    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean isSaved() {
        return id != -1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    //maps a row from the database to this entity
    public static class LibraryRowMapper implements RowMapper<Library> {
        @Override
        public Library mapRow(ResultSet resultSet, int i) throws SQLException {
            return new Library(
                    resultSet.getInt("library_id"),
                    resultSet.getString("name"),
                    resultSet.getString("address"),
                    resultSet.getString("phone_num")
            );
        }
    }

    @Repository
    public static class LibraryDao implements LibraryGateway {
        private static final Logger log = LoggerFactory.getLogger(LibraryDao.class);

        @Autowired
        private JdbcTemplate jdbcTemplate;

        @Override
        public void save(final Library entity) {
            if (entity.isSaved()) {
                log.debug("Updating library " + entity.getId());
                jdbcTemplate.update("update library set name = ?, address = ?, phone_num = ? where library_id = ?",
                        entity.getName(), entity.getAddress(), entity.getPhoneNum(), entity.getId());
            } else {
                log.debug("Inserting library " + entity.getName());
                SimpleJdbcInsert insert = new SimpleJdbcInsert(jdbcTemplate)
                        .withTableName("library")
                        .usingGeneratedKeyColumns("library_id");
                int id = insert.executeAndReturnKey(new HashMap<String, Object>() {{
                    put("name", entity.getName());
                    put("address", entity.getAddress());
                    put("phone_num", entity.getPhoneNum());
                }}).intValue();
                entity.setId(id);
            }
        }

        @Override
        public List<Library> getAll() {
            log.debug("Getting all libraries");
            return jdbcTemplate.query("select * from library", new LibraryRowMapper());
        }

        @Override
        public Library get(int id) {
            log.debug("Getting library by id " + id);
            List<Library> libs = jdbcTemplate.query("select * from library where library_id = ?",
                    new LibraryRowMapper(), id);
            if (libs.size() != 1) {
                log.warn("Could not find library " + id);
                return null;
            }
            return libs.get(0);
        }

        @Override
        public boolean delete(int id) {
            log.debug("Deleting library by id " + id);
            return jdbcTemplate.update("delete from library where library_id = ?", id) == 1;
        }
    }
}
