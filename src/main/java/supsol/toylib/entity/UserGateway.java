package supsol.toylib.entity;

import java.util.List;

//adds more required behaviours to the simple CRUD operations of EntityGateway
public interface UserGateway extends EntityGateway<User> {
    public User getUserByLogin(String email, String passwordHash);
    public List<User> getUsersWithUnpaidFees();
}

