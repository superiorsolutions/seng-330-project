package supsol.toylib.entity;

import java.util.Date;
import java.util.List;

//adds more required behaviours to the simple CRUD operations of EntityGateway
public interface BookingGateway extends EntityGateway<Booking> {
    public Booking isToyBooked(int toyId);
    public List<Booking> getFutureApprovedBookings(int days);
    public List<Booking> getBookingsByUser(int userId);
    public List<Booking> getOverdueBookingsByUser(int userId);
    public List<Booking> getLateReturnsByUser(int userId, int withinDaysAgo);
    public List<Booking> getTentativeBookings();
    public int getTotalDaysBookedSince(int userId, int toyId, Date since);
}
