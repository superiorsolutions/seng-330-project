package supsol.toylib.entity;

import java.util.List;

/* reference: http://martinfowler.com/eaaCatalog/tableDataGateway.html
 * This interface requires each DAO class to implement CRUD operations for persistent entities.
 * This interface is also extended by Gateway interfaces more specific to their particular entity.
 */
public interface EntityGateway<T extends PersistentEntity> {
    public void save(T entity);
    public List<T> getAll();
    public T get(int id);
    public boolean delete(int id);
}
