package supsol.toylib.entity;

/* Represents an entity that is meant to be persisted in the database and thus have a unique identifier.
 * Because of its knowledge of persistence, it also needs to know if it is saved or not--that is, does this entity
 * exist in the persistence storage or is it newly created by application logic.
 */
public interface PersistentEntity {
    public int getId();
    public boolean isSaved();
}
