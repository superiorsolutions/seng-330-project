package supsol.toylib.entity;

import java.util.List;

//adds more required behaviours to the simple CRUD operations of EntityGateway
public interface ToyGateway extends EntityGateway<Toy> {
    public List<Toy> getToysByCategory(int categoryId);
}
