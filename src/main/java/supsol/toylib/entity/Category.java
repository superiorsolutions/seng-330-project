package supsol.toylib.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class Category implements PersistentEntity {

    private int id;
    private String name;

    //public constructor used by views passing the entity to the service layer
    public Category(String name) {
        this.id = -1;
        this.name = name;
    }

    //private constructor that includes the ID, used by this entity's rowmapper
    private Category(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean isSaved() {
        return id != -1;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    //maps a row from the database to this entity
    public static class CategoryRowMapper implements RowMapper<Category> {
        @Override
        public Category mapRow(ResultSet resultSet, int i) throws SQLException {
            return new Category(
                    resultSet.getInt("category_id"),
                    resultSet.getString("name"));
        }
    }

    @Repository
    public static class CategoryDao implements CategoryGateway {
        private static final Logger log = LoggerFactory.getLogger(CategoryDao.class);

        @Autowired
        private JdbcTemplate jdbcTemplate;

        @Override
        public void save(final Category entity) {
            if (entity.isSaved()) {
                log.debug("Updating category " + entity.getId());
                jdbcTemplate.update("update category set name = ? where category_id = ?", entity.getName(),
                        entity.getId());
            } else {
                log.debug("Inserting category " + entity.getName());
                SimpleJdbcInsert insert = new SimpleJdbcInsert(jdbcTemplate)
                        .withTableName("category")
                        .usingGeneratedKeyColumns("category_id");
                int id = insert.executeAndReturnKey(new HashMap<String, Object>() {{
                    put("name", entity.getName());
                }}).intValue();
                entity.setId(id);
            }
        }

        @Override
        public List<Category> getAll() {
            log.debug("Getting all categories");
            return jdbcTemplate.query("select * from category", new CategoryRowMapper());
        }

        @Override
        public Category get(int id) {
            log.debug("Getting category by id " + id);
            List<Category> categories = jdbcTemplate.query("select * from category where category_id = ?",
                    new CategoryRowMapper(), id);
            if (categories.size() != 1) {
                log.warn("Could not find category " + id);
                return null;
            }
            return categories.get(0);
        }

        @Override
        public boolean delete(int id) {
            log.debug("Deleting category by id " + id);
            return jdbcTemplate.update("delete from category where category_id = ?", id) == 1;
        }
    }
}
