package supsol.toylib.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Booking implements PersistentEntity {
    public static enum BookingStatus {
        TENTATIVE, //created by parent
        APPROVED, //created by maintainer
        CANCELLED, //cancelled by staff
        PICKED_UP, //picked up by parent
        MAINTENANCE, //picked up by maintainer
        RETURNED, //returned to the library on time
        RETURNED_LATE //returned by a parent late
    }

    private int id;
    private int toyId;
    private int pickupLibraryId;
    //note: these Dates should be considered dates rather than date-times -- wait for JSR-310 in java 8
    private Date pickupDate;
    private Date returnDate;
    private int userId;
    private BookingStatus status;

    //public constructor used by views passing the entity to the service layer
    public Booking(int toyId, int pickupLibraryId, Date pickupDate, Date returnDate, int userId, BookingStatus status) {
        this.id = -1;
        this.toyId = toyId;
        this.pickupLibraryId = pickupLibraryId;
        this.pickupDate = pickupDate;
        this.returnDate = returnDate;
        this.userId = userId;
        this.status = status;
    }

    //private constructor that includes the ID, used by this entity's rowmapper
    private Booking(int id, int toyId, int pickupLibraryId, Date pickupDate, Date returnDate, int userId, BookingStatus status) {
        this.id = id;
        this.toyId = toyId;
        this.pickupLibraryId = pickupLibraryId;
        this.pickupDate = pickupDate;
        this.returnDate = returnDate;
        this.userId = userId;
        this.status = status;
    }

    @Override
    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean isSaved() {
        return id != -1;
    }

    public int getToyId() {
        return toyId;
    }

    public void setToyId(int toyId) {
        this.toyId = toyId;
    }

    public int getPickupLibraryId() {
        return pickupLibraryId;
    }

    public void setPickupLibraryId(int pickupLibraryId) {
        this.pickupLibraryId = pickupLibraryId;
    }

    public Date getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(Date pickupDate) {
        this.pickupDate = pickupDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public BookingStatus getStatus() {
        return status;
    }

    public void setStatus(BookingStatus status) {
        this.status = status;
    }

    //maps a row from the database to this entity
    public static class BookingRowMapper implements RowMapper<Booking> {
        @Override
        public Booking mapRow(ResultSet resultSet, int i) throws SQLException {
            return new Booking(
                    resultSet.getInt("booking_id"),
                    resultSet.getInt("toy_id"),
                    resultSet.getInt("pickup_library_id"),
                    resultSet.getDate("pickup_date"),
                    resultSet.getDate("return_date"),
                    resultSet.getInt("user_id"),
                    BookingStatus.valueOf(resultSet.getString("status")));
        }
    }

    @Repository
    public static class BookingDao implements BookingGateway {
        private static final Logger log = LoggerFactory.getLogger(BookingDao.class);

        @Autowired
        private JdbcTemplate jdbcTemplate;

        @Override
        public void save(final Booking entity) {
            if (entity.isSaved()) {
                log.debug("Updating booking " + entity.getId());
                jdbcTemplate.update("update booking set toy_id = ?, pickup_library_id = ?, pickup_date = ?, " +
                        "return_date = ?, user_id = ?, status = ? where booking_id = ?", entity.getToyId(),
                        entity.getPickupLibraryId(), entity.getPickupDate(), entity.getReturnDate(), entity.getUserId(),
                        entity.getStatus().name(), entity.getId());
            } else {
                log.debug("Inserting new booking for user " + entity.getUserId());
                SimpleJdbcInsert insert = new SimpleJdbcInsert(jdbcTemplate)
                        .withTableName("booking")
                        .usingGeneratedKeyColumns("booking_id");
                int id = insert.executeAndReturnKey(new HashMap<String, Object>() {{
                    put("toy_id", entity.getToyId());
                    put("pickup_library_id", entity.getPickupLibraryId());
                    put("pickup_date", entity.getPickupDate());
                    put("return_date", entity.getReturnDate());
                    put("user_id", entity.getUserId());
                    put("status", entity.getStatus().name());
                }}).intValue();
                entity.setId(id);
            }
        }

        @Override
        public List<Booking> getAll() {
            log.debug("Getting all bookings");
            return jdbcTemplate.query("select * from booking", new BookingRowMapper());
        }

        @Override
        public Booking get(int bookingId) {
            log.debug("Getting booking by id " + bookingId);
            List<Booking> bookings = jdbcTemplate.query("select * from booking where booking_id = ?",
                    new BookingRowMapper(), bookingId);
            if (bookings.size() != 1) {
                log.warn("Could not find booking with id " + bookingId);
                return null;
            }
            return bookings.get(0);
        }

        @Override
        public boolean delete(int bookingId) {
            log.debug("Deleting booking by id " + bookingId);
            return jdbcTemplate.update("delete from booking where booking_id = ?", bookingId) == 1;
        }

        @Override
        public List<Booking> getBookingsByUser(int userId) {
            log.debug("Getting all bookings for user " + userId);
            return jdbcTemplate.query("select * from booking where user_id = ? order by pickup_date desc",
                    new BookingRowMapper(), userId);
        }

        //gets bookings that have not been returned and the return_date of the booking is before today
        @Override
        public List<Booking> getOverdueBookingsByUser(int userId) {
            log.debug("Getting overdue bookings by user " + userId);
            return jdbcTemplate.query("select * from booking where sysdate > return_date and status = ? and user_id = ?",
                    new BookingRowMapper(), BookingStatus.PICKED_UP.name(), userId);
        }

        //gets bookings that the user has returned late within a certain number of days
        @Override
        public List<Booking> getLateReturnsByUser(int userId, int withinDaysAgo) {
            log.debug("Getting bookings returned late by user " + userId);
            return jdbcTemplate.query("select * from booking where status = ? and user_id = ? " +
                    "and return_date > dateadd('DAY', -?, sysdate) order by return_date desc",
                    new BookingRowMapper(), BookingStatus.RETURNED_LATE.name(), userId, withinDaysAgo);
        }

        //gets the cumulative number of days since sinceDate that the given user has booked the given toy
        @Override
        public int getTotalDaysBookedSince(int userId, int toyId, Date sinceDate) {
            final int[] days = new int[1]; //hack to allocate the int on heap
            jdbcTemplate.query("select pickup_date, return_date from booking where user_id = ? and toy_id = ? " +
                    "and status = ? and pickup_date > ?",
                    new RowCallbackHandler() {
                        @Override
                        public void processRow(ResultSet resultSet) throws SQLException {
                            Date start = resultSet.getDate("pickup_date");
                            Date end = resultSet.getDate("dropoff_date");
                            days[0] += (int) TimeUnit.MILLISECONDS.toDays(end.getTime() - start.getTime());
                        }
                    }, userId, toyId, BookingStatus.RETURNED.name(), sinceDate);
            return days[0];
        }

        //gets all bookings that have not been approved by staff yet
        @Override
        public List<Booking> getTentativeBookings() {
            log.debug("Getting tentative bookings");
            return jdbcTemplate.query("select * from booking where status = ? order by pickup_date asc",
                    new BookingRowMapper(), BookingStatus.TENTATIVE.name());
        }

        @Override
        public Booking isToyBooked(int toyId) {
            log.debug("determining if toy " + toyId + " is booked");
            List<Booking> bookings = jdbcTemplate.query("select * from booking where toy_id = ? and status = ? " +
                    "and pickup_date <= sysdate and return_date > sysdate", new BookingRowMapper(), toyId,
                    BookingStatus.PICKED_UP.name());
            if (bookings.size() > 0) {
                return bookings.get(0);
            }
            return null;
        }

        //get approved bookings in the near future, used by the library service to determine which toys need to be moved
        @Override
        public List<Booking> getFutureApprovedBookings(int days) {
            log.debug("Getting future bookings within " + days + " days");
            return jdbcTemplate.query("select * from booking where pickup_date >= sysdate " +
                    "and pickup_date <= dateadd('DAY', ?, sysdate) and status = ? order by pickup_date desc",
                    new BookingRowMapper(), days, BookingStatus.APPROVED.name());
        }
    }
}
