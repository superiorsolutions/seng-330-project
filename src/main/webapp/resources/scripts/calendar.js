var today = new Date();
var iterator = new Date();
var monthDelta = 0;
var pickingStart = true;
var pickingEnd = false;
var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
window.onload = function() {
	draw();
};
function draw() {
	for (var i = 0; i < 6; i++) {
		for (var j = 0;j < 7;j++) {
				$("#"+i+j).text("");
				$("#"+i+j).attr("onclick","");
		}
	}
	var numDate = 1;
	iterator.setFullYear(today.getFullYear(), today.getMonth()+monthDelta, numDate);
	var row=0;
	var col=iterator.getDay();
	$("#"+row+col).text(numDate);
	if (numDate == today.getDate() && today.getMonth() == iterator.getMonth() && today.getFullYear() == iterator.getFullYear()) $("#"+row+col).css("color","red");
	else $("#"+row+col).css("color","black");
	if (numDate < today.getDate() && today.getMonth() == iterator.getMonth() && today.getFullYear() == iterator.getFullYear()){
		$("#"+row+col).css("background-color", "gray");
	} else {
		$("#"+row+col).attr("onclick", "pickDate(" + iterator.getFullYear().toString().slice(2) + "," + iterator.getMonth() + "," + numDate + ")");
		$("#"+row+col).css("background-color", "white");
	}
	var nextDay = new Date();
	nextDay.setFullYear(iterator.getFullYear(),iterator.getMonth(),iterator.getDate()+1);
	while (nextDay.getDate() != 1) {
		iterator.setDate(iterator.getDate()+1);
		nextDay.setDate(nextDay.getDate()+1);
		col = iterator.getDay();
		numDate++;
		if (col == 0) {
			row++;
		}
		$("#"+row+col).text(numDate);
		if (numDate == today.getDate() && today.getMonth() == iterator.getMonth() && today.getFullYear() == iterator.getFullYear()) $("#"+row+col).css("color","red");
		else $("#"+row+col).css("color","black");
		if (numDate < today.getDate() && today.getMonth() == iterator.getMonth() && today.getFullYear() == iterator.getFullYear()){
			$("#"+row+col).css("background-color", "gray");
		} else {
			$("#"+row+col).attr("onclick", "pickDate(" + iterator.getFullYear().toString().slice(2) + "," + iterator.getMonth() + "," + numDate + ")");
			$("#"+row+col).css("background-color", "white");
		}
	}
	$("#month").text(monthNames[iterator.getMonth()] + " " + iterator.getFullYear());
	for (var i = 0;i < 6;i++) {
		for (var j = 0;j < 7;j++) {
			if ($("#"+i+j).text()=="") {
				$("#"+i+j).css("background-color", "black");
			} /*else {
				$("#"+i+j).css("background-color", "white");
			}*/
		}
	}
	if (monthDelta==0) {
		$("#prevMonth").html("");
	} else {
		$("#prevMonth").html("&larr;");
	}
}
function prevMonth() {
	if (monthDelta!=0) {
		monthDelta--;
		draw();
	}
}
function nextMonth() {
	monthDelta++;
	draw();
}
function pickDate(year, month, day) {
	month++;
	if (pickingStart) {
		$("#startDate").val(day + "/" + month + "/" + year);
		pickingStart = false;
		pickingEnd = true;
	} else if (pickingEnd) {
		$("#endDate").val(day + "/" + month + "/" + year);
		pickingStart = false;
		pickingEnd = false;
	}
	console.log (year + " " + month + " " + day);
}
function pickStart() {
	pickingStart = true;
	pickingEnd = false;
}
function pickEnd() {
	pickingSTart = false;
	pickingEnd = true;
}